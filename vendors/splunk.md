# Log Management: Splunk


```shell
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-splunk

scp $HOME/Downloads/splunk-9.0.0-6818ac46f2ec-linux-2.6-amd64.deb root@65.21.0.6:/root
ssh root@65.21.0.6

apt install ./splunk-9.0.0-6818ac46f2ec-linux-2.6-amd64.deb

cd /opt/splunk/bin/
./splunk start
q
Do you agree with this license? [y/n]: y
Please enter an administrator username: admin
Please enter a new password:
Please confirm new password:

The Splunk web interface is at http://ix-log-splunk:8000
```
