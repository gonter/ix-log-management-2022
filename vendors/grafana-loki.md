# Log Management: Grafana Loki

Für diesen Beitrag wurde Grafana 8 installiert, da sich in Grafana 9.0.2 der Access-Typ nicht auf `proxy` konfigurieren liess, um Loki nur auf localhost als Datenquelle zu verwenden. Aus Sicherheitsgründen sollte man Daten-APIs nicht für das Internet (den Browser-Client) verfügbar machen. Loki stellt Metriken für Prometheus bereit. Dieser Endpunkt kann dazu verwendet werden, um die Installation zu überprüfen: `curl localhost:3100/metrics`. Um Logs in Loki einzuliefern, muss Promtail installiert und konfiguriert werden. Loki als Datasource kann sowohl in der Grafana-Weboberfläche, als auch als lokale Provisionsquelle konfiguriert werden. Letzteres bietet sich Infrastructure-as-Code und GitOps Workflows an. Hinweis: `access: proxy` steht nicht in der UI zur Verfügung, lediglich als Provisionierungsoption. 

```shell 
hcloud server create --image ubuntu-20.04 --type cx21 --name ix-log-loki

ssh root@... # oder ssh user@ und sudo -i

# Grafana Installation
apt update && apt install -y gnupg2 curl
curl https://packages.grafana.com/gpg.key | apt-key add -
add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt -y install grafana=8.5.6
systemctl start grafana-server
systemctl enable grafana-server
ufw allow proto tcp from any to any port 3000
curl -s localhost:3000/login  | grep '<title>Grafana'

# Loki Installation (die ersten beiden Befehle berechnen die aktuelle Release-Version)
apt install jq unzip
LOKI_VERSION=$(curl -sL https://api.github.com/repos/grafana/loki/releases/latest | jq -r ".tag_name")
wget https://github.com/grafana/loki/releases/download/$LOKI_VERSION/loki-linux-amd64.zip
unzip loki-linux-amd64.zip
mv loki-linux-amd64 /usr/local/bin/loki
mkdir -p /etc/loki /var/lib/loki

cat >/etc/loki/loki-local-config.yaml<<EOF
# Local configuration, example in https://grafana.com/docs/loki/latest/configuration/examples/#complete-local-configyaml 
auth_enabled: false

server:
  http_listen_port: 3100

ingester:
  lifecycler:
    address: 127.0.0.1
    ring:
      kvstore:
        store: inmemory
      replication_factor: 1
    final_sleep: 0s
  chunk_idle_period: 5m
  chunk_retain_period: 30s

schema_config:
  configs:
  - from: 2020-05-15
    store: boltdb
    object_store: filesystem
    schema: v11
    index:
      prefix: index_
      period: 168h

storage_config:
  boltdb:
    directory: /var/lib/loki/index

  filesystem:
    directory: /var/lib/loki/chunks

limits_config:
  enforce_metric_name: false
  reject_old_samples: true
  reject_old_samples_max_age: 168h
EOF

cat >/etc/systemd/system/loki.service<<EOF
[Unit]
Description=Loki service
After=network.target

[Service]
Type=simple
User=root
ExecStart=/usr/local/bin/loki -config.file /etc/loki/loki/local-config.yaml

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable loki 
systemctl start loki
systemctl status loki 

# Promtail installation
apt install jq unzip
LOKI_VERSION=$(curl -sL https://api.github.com/repos/grafana/loki/releases/latest | jq -r ".tag_name")
wget https://github.com/grafana/loki/releases/download/$LOKI_VERSION/promtail-linux-amd64.zip
unzip promtail-linux-amd64.zip
mv promtail-linux-amd64 /usr/local/bin/promtail 

cat >/etc/loki/promtail-local-config.yaml<<EOF
server:
  http_listen_port: 9080
  grpc_listen_port: 0

positions:
  filename: /var/lib/loki/positions.yaml

clients:
  - url: http://localhost:3100/loki/api/v1/push

scrape_configs:
- job_name: system
  static_configs:
  - targets:
      - localhost
    labels:
      job: varlogs
      __path__: /var/log/*log
EOF

$ cat >/etc/systemd/system/promtail.service<<EOF
[Unit]
Description=Promtail service
After=network.target

[Service]
Type=simple
User=root
ExecStart=/usr/local/bin/promtail -config.file /etc/loki/promtail-local-config.yaml

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload 
systemctl enable promtail
systemctl start promtail
systemctl status promtail

# Grafana Datasource
cat >/etc/grafana/provisioning/datasources/loki.yaml<<EOF

# https://grafana.com/docs/grafana/latest/administration/provisioning/
apiVersion: 1

datasources:
  - name: loki-demo
    type: loki
    access: proxy # kein direkter Zugriff vom Client-Browser
    url: http://localhost:3100
EOF 

chown root:grafana /etc/grafana/provisioning/datasources/loki.yaml

systemctl restart grafana-server     
```   
