# Tests

Um den Log-Sammlungs-Workflow zu testen wurden Logs mit dem Betriff `Error` mit Hilfe des `logger` Befehl generiert. Das Linux-Syslog muss von der Applikation eingelesen werden, und die Suche in den Dashboards die entsprechenden Werte zurückgeben. Alternativ kann man einen Nginx-Webserver installieren, und mittels curl-Endlosschleife ein `access.log` generieren, welches die Log-Management-Tools dann auswerten sollen.

```
logger System <message>

logger System Error: Filebeat not working

# 1. Terminal: Linux VM, mit Log-Collector
apt install nginx

# 2. Terminal
while true; do curl http://65.108.241.97 && sleep 1; done
```
